<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Controllers\CharacterController;
use App\Http\Controllers\CharacterDeskripsiController;
use App\Http\Controllers\CharacterDibangunController;
use App\Http\Controllers\UserController;
use App\Models\Character;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [UserController::class, 'welcome'])->middleware('auth')->name('welcome');
Route::get('/profile', [UserController::class, 'profile'])->middleware('auth')->name('profile');
Route::post('/avatar-post', [UserController::class, 'update_profile'])->middleware('auth')->name('profile.post');
Route::post('/change-passowrd', [ChangePasswordController::class, 'changePassword'])->name('change.password');
Route::get('/print-character/{id}',[CharacterController::class, 'cetak'])->middleware('auth')->name('cetak');
Route::get('/characters/export/', [CharacterController::class, 'export'])->name('export');

Route::get('/karakter/rombel', [CharacterController::class, 'rombel'])->middleware('auth')->name('rombel');

Route::get('/karakter/kelas-bdp', [CharacterController::class, 'kelas_bdp'])->middleware('auth')->name('kelas.bdp');
Route::get('/karakter/kelas-mmd', [CharacterController::class, 'kelas_mmd'])->middleware('auth')->name('kelas.mmd');
Route::get('/karakter/kelas-otkp', [CharacterController::class, 'kelas_otkp'])->middleware('auth')->name('kelas.otkp');
Route::get('/karakter/kelas-htl', [CharacterController::class, 'kelas_htl'])->middleware('auth')->name('kelas.htl');
Route::get('/karakter/kelas-rpl', [CharacterController::class, 'kelas_rpl'])->middleware('auth')->name('kelas.rpl');
Route::get('/karakter/kelas-tbg', [CharacterController::class, 'kelas_tbg'])->middleware('auth')->name('kelas.tbg');
Route::get('/karakter/kelas-tkj', [CharacterController::class, 'kelas_tkj'])->middleware('auth')->name('kelas.tkj');

Route::get('/karakter/kelas-bdp-x', [CharacterController::class, 'kelas_x_bdp'])->middleware('auth')->name('kelas.x.bdp');
Route::get('/karakter/kelas-bdp-xi', [CharacterController::class, 'kelas_xi_bdp'])->middleware('auth')->name('kelas.xi.bdp');
Route::get('/karakter/kelas-bdp-xii', [CharacterController::class, 'kelas_xii_bdp'])->middleware('auth')->name('kelas.xii.bdp');

Route::get('/karakter/kelas-htl-x', [CharacterController::class, 'kelas_x_htl'])->middleware('auth')->name('kelas.x.htl');
Route::get('/karakter/kelas-htl-xi', [CharacterController::class, 'kelas_xi_htl'])->middleware('auth')->name('kelas.xi.htl');
Route::get('/karakter/kelas-htl-xii', [CharacterController::class, 'kelas_xii_htl'])->middleware('auth')->name('kelas.xii.htl');

Route::get('/karakter/kelas-mmd-x', [CharacterController::class, 'kelas_x_mmd'])->middleware('auth')->name('kelas.x.mmd');
Route::get('/karakter/kelas-mmd-xi', [CharacterController::class, 'kelas_xi_mmd'])->middleware('auth')->name('kelas.xi.mmd');
Route::get('/karakter/kelas-mmd-xii', [CharacterController::class, 'kelas_xii_mmd'])->middleware('auth')->name('kelas.xii.mmd');

Route::get('/karakter/kelas-otkp-x', [CharacterController::class, 'kelas_x_otkp'])->middleware('auth')->name('kelas.x.otkp');
Route::get('/karakter/kelas-otkp-xi', [CharacterController::class, 'kelas_xi_otkp'])->middleware('auth')->name('kelas.xi.otkp');
Route::get('/karakter/kelas-otkp-xii', [CharacterController::class, 'kelas_xii_otkp'])->middleware('auth')->name('kelas.xii.otkp');

Route::get('/karakter/kelas-rpl-x', [CharacterController::class, 'kelas_x_rpl'])->middleware('auth')->name('kelas.x.rpl');
Route::get('/karakter/kelas-rpl-xi', [CharacterController::class, 'kelas_xi_rpl'])->middleware('auth')->name('kelas.xi.rpl');
Route::get('/karakter/kelas-rpl-xii', [CharacterController::class, 'kelas_xii_rpl'])->middleware('auth')->name('kelas.xii.rpl');

Route::get('/karakter/kelas-tbg-x', [CharacterController::class, 'kelas_x_tbg'])->middleware('auth')->name('kelas.x.tbg');
Route::get('/karakter/kelas-tbg-xi', [CharacterController::class, 'kelas_xi_tbg'])->middleware('auth')->name('kelas.xi.tbg');
Route::get('/karakter/kelas-tbg-xii', [CharacterController::class, 'kelas_xii_tbg'])->middleware('auth')->name('kelas.xii.tbg');

Route::get('/karakter/kelas-tkj-x', [CharacterController::class, 'kelas_x_tkj'])->middleware('auth')->name('kelas.x.tkj');
Route::get('/karakter/kelas-tkj-xi', [CharacterController::class, 'kelas_xi_tkj'])->middleware('auth')->name('kelas.xi.tkj');
Route::get('/karakter/kelas-tkj-xii', [CharacterController::class, 'kelas_xii_tkj'])->middleware('auth')->name('kelas.xii.tkj');


Route::resource('characters', CharacterController::class)->middleware('auth');
Route::resource('character-dibangun', CharacterDibangunController::class)->middleware('auth');
