<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToCharacters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('characters', function (Blueprint $table) {
            $table->unsignedBigInteger('alias_1');
            $table->unsignedBigInteger('alias_2');
            $table->unsignedBigInteger('alias_3');
            $table->unsignedBigInteger('alias_4');
            $table->unsignedBigInteger('alias_5');


            $table->foreign('alias_1')->references('id')->on('character_dibanguns')->onUpdate('cascade')->onDelete('cascade');


            $table->foreign('alias_2')->references('id')->on('character_dibanguns')->onUpdate('cascade')->onDelete('cascade');


            $table->foreign('alias_3')->references('id')->on('character_dibanguns')->onUpdate('cascade')->onDelete('cascade');


            $table->foreign('alias_4')->references('id')->on('character_dibanguns')->onUpdate('cascade')->onDelete('cascade');


            $table->foreign('alias_5')->references('id')->on('character_dibanguns')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('characters', function (Blueprint $table) {
            $table->dropForeign(['alias_1']);
            $table->dropColumn(['alias_1']);

            $table->dropForeign(['alias_2']);
            $table->dropColumn(['alias_2']);

            $table->dropForeign(['alias_3']);
            $table->dropColumn(['alias_3']);

            $table->dropForeign(['alias_4']);
            $table->dropColumn(['alias_4']);

            $table->dropForeign(['alias_5']);
            $table->dropColumn(['alias_5']);
        });
    }
}
