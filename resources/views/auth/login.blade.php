@extends('layouts.login')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <div class="auth-wrapper auth-v2">
          <div class="auth-inner row m-0">
            <!-- Brand logo--><a class="brand-logo" href="#">
              <img src="../../app-assets/images/logowk.png" alt="logo wk" height="50">
              <h2 class="brand-text text-primary ml-1 pt-1">Wikrama</h2></a>
            <!-- /Brand logo-->
            <!-- Left Text-->
            <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
              <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid" src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/pages/login-v2.svg" alt="Login V2"/></div>
            </div>
            <!-- /Left Text-->
            <!-- Login-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
              <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Welcome to Wikrama! 👋</h2>
                <p class="card-text mb-2">Please sign-in to your account</p>
                <form class="auth-login-form mt-2" method="POST" action="{{ route('login') }}">
                    @csrf
                    @if ($message = Session::get('success'))
                    <div class="">
                      <div class="alert alert-success mb-50" role="alert">
                        <div class="alert-body">
                          <p class="alert">{{ $message }}</p>
                        </div>
                      </div>
                    </div>
                    @endif
                  <div class="form-group">
                    <label class="form-label" for="login-email">Email</label>
                    <input class="form-control @error('email') is-invalid @enderror" id="login-email" type="email" name="email" placeholder="Insert your email here" aria-describedby="login-email" value="{{ old('email') }}" required autocomplete="email" autofocus tabindex="1"/>
                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="d-flex justify-content-between">
                    @if (Route::has('password.request'))
                      <label for="login-password">Password</label><a href="{{ route('password.request') }}"><small>Forgot Password?</small></a>
                    @endif
                    </div>
                    <div class="input-group input-group-merge form-password-toggle">
                      <input class="form-control form-control-merge @error('password') is-invalid @enderror" id="login-password" type="password" name="password" placeholder="Insert your password here" required autocomplete="current-password" aria-describedby="login-password" tabindex="2"/>
                      <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                      @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input class="custom-control-input" id="remember-me" name="remember" {{ old('remember') ? 'checked' : '' }} type="checkbox" tabindex="3"/>
                      <label class="custom-control-label" for="remember-me"> Remember Me</label>
                    </div>
                  </div>
                  <button class="btn btn-primary btn-block" type="submit" tabindex="4">Sign in</button>
                </form>
                <p class="text-center mt-2"><span>Don't have account?</span><a href="{{ route('register') }}"><span>&nbsp;Create an account</span></a></p>
              </div>
            </div>
            <!-- /Login-->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
