@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Karakter Dibangun Table</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Table Karakter Dibangun
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
          </div>
        </div>
      </div>


<!-- Table Hover Animation start -->
<div class="row" id="table-hover-animation">
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">List Karakter Dibangun</h4>

      <a href="{{ route('character-dibangun.create') }}" class="btn btn-success">+ Tambah</a>

    </div>
    <form class="d-flex" action="{{ route('character-dibangun.index') }}" method="GET">
        {{ csrf_field() }}
        <div class="col-2"><div><input type="text" class="form-control" name="search" value="{{ request()->query('search') }}" placeholder="Cari . . . "></div></div>
        <button class="btn btn-primary"><i data-feather="search"></i></button>
        {{-- <a class="btn btn-dark ag-grid-export-btn ml-auto mr-2" href="{{ route('export') }}">Export as Excel</a> --}}
    </form>

    <div class="card-body">
    </div>

    <div class="table-responsive">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
      <table class="table table-hover-animation">
        <thead>
          <tr>
            <th>No</th>
            <th width="500px">Alias</th>
            <th width="500px">Judul</th>
            <th width="500px">Deskripsi</th>
            <th width="500px">Actions</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($characters as $character)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $character->alias }}</td>
                <td>{{ $character->judul }}</td>
                <td>{{ $character->deskripsi }}</td>
                <td>
                        <form action="{{ route('character-dibangun.destroy',$character->id) }}" method="POST">
                                  <a class="btn btn-warning" href="{{ route('character-dibangun.edit', $character->id) }}">
                                    <i data-feather="edit-2" class="mr-50"></i>
                                    <span>Edit</span>
                                  </a>

                                  @csrf
                                  @method('DELETE')
                                  <button class="btn btn-danger"><i data-feather="trash" class="mr-50"></i><span>Delete</span></button>
                        </form>
                </td>
              </tr>
            @empty
            <tr>
                <td colspan="10" class="text-center">
                    <p class="text-center">No result found for query <strong>{{ request()->query('search') }}</strong></p>
                </td>
            </tr>
            @endforelse
        </tbody>
      </table>
    </div>

  </div>
  {!! $characters->appends(['search' => request()->query('search')])->links() !!}
</div>
</div>
<!-- Table head options end -->

      </div>
    </div>
  </div>
@endsection
