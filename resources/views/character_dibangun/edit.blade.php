@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Form Validation</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Forms</a>
                  </li>
                  <li class="breadcrumb-item active">Form Validation
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
            <div class="dropdown">
              <a class="btn-icon btn btn-danger btn-round btn-sm dropdown-toggle" href="{{ route('character-dibangun.index') }}">Back</a>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Validation -->
<section class="bs-validation">
<div class="row">
  <!-- Bootstrap Validation -->
  <div class="col-md-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Bootstrap Validation</h4>
      </div>
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form class="needs-validation" method="POST" action="{{ route('character-dibangun.update', $character->id) }}">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="form-group col-md-6">
                    <label class="form-label" for="alias">Alias</label>

                    <input
                      type="text"
                      id="alias"
                      class="form-control"
                      placeholder="Masukkan Alias"
                      name="alias"
                      aria-describedby="basic-addon-name"
                      value="{{ $character->alias }}"
                      required
                    />
                </div>
                <div class="form-group col-md-6">
                    <label class="form-label" for="judul">Judul</label>

                    <input
                      type="text"
                      id="judul"
                      class="form-control"
                      placeholder="Masukkan Judul"
                      name="judul"
                      aria-describedby="basic-addon-name"
                      value="{{ $character->judul }}"
                      required
                    />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="form-label" for="deskripsi">Deskripsi</label>

                    <textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="10" placeholder="Masukkan Deskripsi">{{ $character->deskripsi }}</textarea>
                </div>
            </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /Bootstrap Validation -->
</div>
</section>
<!-- /Validation -->

      </div>
    </div>
  </div>
@endsection
