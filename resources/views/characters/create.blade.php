@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Form Validation</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">Forms</a>
                  </li>
                  <li class="breadcrumb-item active">Form Validation
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
            <div class="dropdown">

            </div>
          </div>
        </div>
      </div>
      <div class="content-body"><!-- Validation -->
<section class="bs-validation">
<div class="row">
  <!-- Bootstrap Validation -->
  <div class="col-md-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Bootstrap Validation</h4>
      </div>
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form class="needs-validation" method="POST" action="{{ route('characters.store') }}">
            @csrf
            <div class="form-group">
                <label class="form-label" for="basic-addon-name">Nomor Induk Siswa(NIS)</label>

                <input
                  type="number"
                  id="basic-addon-name"
                  class="form-control"
                  placeholder="Nomor Induk"
                  name="nis"
                  aria-describedby="basic-addon-name"
                  required
                />
              </div>
              <div class="form-group">
                <label class="form-label" for="basic-default-email1">Nama Lengkap</label>
                <input
                  type="text"
                  id="basic-default-email1"
                  class="form-control"
                  name="nama"
                  placeholder="Nama Lengkap"
                  required
                />
              </div>
          <div class="form-group">
            <label class="form-label" for="alias_1">Alias 1</label>
            <select class="form-control" name="alias_1" id="alias_1" required>
                <option value="" disabled selected>Pilih Alias 1</option>
                @foreach ($juduls as $judul)
                <option value="{{ $judul->id }}">{{ $judul->alias }}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="alias_1">Alias 2</label>
            <select class="form-control" name="alias_2" id="alias_2" required>
                <option value="" disabled selected>Pilih Alias 1</option>
                @foreach ($juduls as $judul)
                <option value="{{ $judul->id }}">{{ $judul->alias }}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="alias_3">Alias 3</label>
            <select class="form-control" name="alias_3" id="alias_3" required>
                <option value="" disabled selected>Pilih Alias 3</option>
                @foreach ($juduls as $judul)
                <option value="{{ $judul->id }}">{{ $judul->alias }}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="alias_1">Alias 4</label>
            <select class="form-control" name="alias_4" id="alias_4" required>
                <option value="" disabled selected>Pilih Alias 4</option>
                @foreach ($juduls as $judul)
                <option value="{{ $judul->id }}">{{ $judul->alias }}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="alias_5">Alias 5</label>
            <select class="form-control" name="alias_5" id="alias_5" required>
                <option value="" disabled selected>Pilih Alias 5</option>
                @foreach ($juduls as $judul)
                <option value="{{ $judul->id }}">{{ $judul->alias }}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="catatan">Catatan</label>
            <select class="form-control" name="catatan" id="catatan" required>
                <option value="" disabled selected>Pilih Deskripsi Catatan</option>
                <option value="Ananda menunjukkan perkembangan karakter yang BAIK pada pembelajaran semester ini.">Ananda menunjukkan perkembangan karakter yang BAIK pada pembelajaran semester ini.</option>
                <option value="Ananda menunjukkan perkembangan karakter yang SANGAT BAIK pada pembelajaran semester ini.">Ananda menunjukkan perkembangan karakter yang SANGAT BAIK pada pembelajaran semester ini.</option>
                <option value="Ananda menunjukkan perkembangan karakter yang CUKUP BAIK pada pembelajaran semester ini.">Ananda menunjukkan perkembangan karakter yang CUKUP BAIK pada pembelajaran semester ini.</option>
                <option value="Ananda menunjukkan perkembangan karakter yang KURANG BAIK pada pembelajaran semester ini.">Ananda menunjukkan perkembangan karakter yang KURANG BAIK pada pembelajaran semester ini.</option>
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="penerbitan">Tanggal Penerbitan</label>
            <input
            type="date"
            id="penerbitan"
            class="form-control"
            placeholder="Tanggal Penerbitan"
            name="penerbitan"
            aria-describedby="basic-addon-name"
            required
          />
          </div>
          <div class="form-group">
            <label class="form-label" for="Jurusan">Jurusan</label>
            <select class="form-control" name="jurusan" id="Jurusan" required>
                <option value="" disabled selected>Pilih Jurusan</option>
                <option value="Bisnis Daring dan Pemasaran">Bisnis Daring dan Pemasaran</option>
                <option value="Multimedia">Multimedia</option>
                <option value="Otomatisasi Tatakelola dan Perkantoran">Otomatisasi Tatakelola dan Perkantoran</option>
                <option value="Perhotelan">Perhotelan</option>
                <option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
                <option value="Tataboga">Tataboga</option>
                <option value="Teknik Komputer dan Jaringan">Teknik Komputer dan Jaringan</option>
              </select>
          </div>
          <div class="form-group">
            <label class="form-label" for="kelas">Kelas</label>
            <select class="form-control" name="kelas" id="kelas" required>
                <option value="" disabled selected>Pilih Kelas</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
              </select>
          </div>
          <div class="alert alert-danger mt-1 alert-validation-msg" role="alert">
            <div class="alert-body">
              <i data-feather="info" class="mr-50 align-middle"></i>
              <span><strong>Note:</strong> Harap memilih semua pilihan lagi karena web ini masih dalam tahap test beta!</span>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /Bootstrap Validation -->
</div>
</section>
<!-- /Validation -->

      </div>
    </div>
  </div>
@endsection
