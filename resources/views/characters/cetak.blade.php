<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        table.static {
            position: relative;
            border: 2px solid;
        }
        .border-wd {
            border: 2px solid;
        }
        .text-status{
            font-weight:500;
        }
        .text-category{
            font-weight: 400;
        }
        .code-qr {
            text-align: center;
            padding-top: 150px;
            padding-bottom: 150px;
        }
        .student{
            padding-left: 80px;
        }
        .group-student{
            margin-top: 205px;
        }
        .student-custom{
            font-weight: 500;
        }
    </style>
    <title>Print Character</title>
</head>
<body>
    <div class="container mt-2">
        <span class="pull-left"><b>F. Deskripsi Perkembangan Karakter</b></span>
        <table class="static" width="100%">
            <thead class="border-wd">
                <tr>
                  <th style="padding-right: 20px; padding-left: 20px" class="border-wd text-center">Karakter yang dibangun</th>
                  <th class="border-wd text-center">Deskripsi</th>
                </tr>
              </thead>
              <tbody class="border-wd">
                  <tr class="border-wd">
                      <td class="border-wd text-status text-center" style="padding: 20px;">{{ $character->alias1->judul }}</td>
                      <td class="border-wd text-category px-2">{{ $character->alias1->deskripsi }}</td>
                  </tr>
                  <tr class="border-wd">
                      <td class="border-wd text-status text-center" style="padding: 20px;">{{ $character->alias2->judul }}</td>
                      <td class="border-wd text-category px-2">{{ $character->alias2->deskripsi }}</td>
                  </tr>
                  <tr class="border-wd">
                    <td class="border-wd text-status text-center" style="padding: 20px;">{{ $character->alias3->judul }}</td>
                    <td class="border-wd text-category px-2">{{ $character->alias3->deskripsi }}</td>
                </tr class="border-wd">
                <tr>
                    <td class="border-wd text-status text-center" style="padding: 20px;">{{ $character->alias4->judul }}</td>
                    <td class="border-wd text-category px-2">{{ $character->alias4->deskripsi }}</td>
                </tr class="border-wd">
                <tr class="border-wd">
                    <td class="border-wd text-status text-center" style="padding: 20px;">{{ $character->alias5->judul }}</td>
                    <td class="border-wd text-category px-2">{{ $character->alias5->deskripsi }}</td>
                </tr>
              </tbody>
        </table>
    </div>
    <div class="container mt-4">
        <span class="pull-left"><b>G. Catatan Perkembangan Karakter</b></span>
        <table class="static" width="100%">
              <tbody class="border-wd">
                  <tr class="border-wd">
                      <td class="border-wd text-category px-2 py-5">{{ $character->catatan }}</td>
                  </tr>
              </tbody>
        </table>
    </div>
    <div class="container mt-4">
        <table class="static" width="100%">
              <tbody class="border-wd">
                  <tr class="border-wd">
                      <td class="border-wd text-category px-2 pt-2">Laporan ini diterbitkan oleh SMK Wikrama Bogor dan dicetak secara digital pada tanggal {{ Carbon\Carbon::parse($character->penerbitan)->isoFormat('D MMMM Y') }}

                          <p class="code-qr">{!! QrCode::size(300)->generate(Request::url()); !!}</p>

                      </td>
                  </tr>
              </tbody>
        </table>
    </div>
    <div class="group-student container">
        <p class="student-custom"><span>{{ $character->nis }}</span><span class="student">{{ $character->nama }}</span></p>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script>
        window.print();
    </script>
</body>
</html>
