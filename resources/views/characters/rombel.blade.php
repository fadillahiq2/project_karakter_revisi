@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Pilih Jurusan</h2>
            </div>
          </div>
        </div>
      </div>

            <div class="text-center">
                <a class="btn btn-primary btn-lg col-md-8 mt-5" href="{{ route('kelas.bdp') }}">Bisnis Daring dan Pemasaran</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.mmd') }}">Multimedia</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.otkp') }}">Otomatisasi Tatakelola dan Perkantoran</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.htl') }}">Perhotelan</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.rpl') }}">Rekayasa Perangkat Lunak</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.tbg') }}">Tataboga</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.tkj') }}">Teknik Komputer dan Jaringan</a>
            </div>


      </div>
    </div>
  </div>
@endsection
