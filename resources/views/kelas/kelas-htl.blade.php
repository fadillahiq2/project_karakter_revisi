@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-12 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Pilih Jurusan</h2>
              <a class="btn btn-primary btn-sm float-right" href="{{ route('rombel') }}">Back</a>
            </div>
          </div>
        </div>
      </div>

            <div class="text-center">
                <a class="btn btn-primary btn-lg col-md-8 mt-5" href="{{ route('kelas.x.htl') }}">Kelas X</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.xi.htl') }}">Kelas XI</a>

                <a class="btn btn-primary btn-lg col-md-8 mt-2" href="{{ route('kelas.xii.htl') }}">Kelas XII</a>
            </div>


      </div>
    </div>
  </div>
@endsection
