@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h2 class="content-header-title float-left mb-0">Karakter Tables</h2>
              <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('welcome') }}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Table Karakter
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
          <div class="form-group breadcrumb-right">
          </div>
        </div>
      </div>


<!-- Table Hover Animation start -->
<div class="row" id="table-hover-animation">
<div class="col-12">
  <div class="card">
    <div class="card-header">
      <h4 class="card-title">List Karakter</h4>

      <a href="{{ route('characters.create') }}" class="btn btn-success">+ Tambah</a>

    </div>
    <form class="d-flex" action="{{ route('kelas.xi.rpl') }}" method="GET">
        {{ csrf_field() }}
        <div class="col-2"><div><input type="text" class="form-control" name="search" value="{{ request()->query('search') }}" placeholder="Cari nis siswa"></div></div>
        <button class="btn btn-primary"><i data-feather="search"></i></button>
        <a class="btn btn-dark ag-grid-export-btn ml-auto mr-2" href="{{ route('export') }}">Export as Excel</a>
    </form>

    <div class="card-body">
    </div>

    <div class="table-responsive">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
      <table class="table table-hover-animation">
        <thead>
          <tr>
            <th>NIS</th>
            <th>Nama</th>
            <th>Alias 1</th>
            <th>Alias 2</th>
            <th>Alias 3</th>
            <th>Alias 4</th>
            <th>Alias 5</th>
            <th>Catatan</th>
            <th>Tanggal Terbit</th>
            <th>Jurusan</th>
            <th>Kelas</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($characters as $character)
            <tr>
                <td>{{ $character->nis }}</td>
                <td>{{ $character->nama }}</td>
                <td>{{ $character->alias1->alias }}</td>
                <td>{{ $character->alias2->alias }}</td>
                <td>{{ $character->alias3->alias }}</td>
                <td>{{ $character->alias4->alias }}</td>
                <td>{{ $character->alias5->alias }}</td>
                <td>{{ $character->catatan }}</td>
                <td>{{ Carbon\Carbon::parse($character->penerbitan)->isoFormat('D MMMM Y') }}</td>
                <td>{{ $character->jurusan }}</td>
                <td>{{ $character->kelas }}</td>
                <td>
                        <form action="{{ route('characters.destroy',$character->id) }}" method="POST">
                                  <a class="btn btn-warning" href="{{ route('characters.edit',$character->id) }}">
                                    <i data-feather="edit-2" class="mr-50"></i>
                                    <span>Edit</span>
                                  </a>
                                  <a class="btn btn-success" href="{{ route('cetak',$character->id) }}">
                                    <i data-feather="upload" class="mr-50"></i>
                                    <span>Print</span>
                                  </a>

                                  @csrf
                                  @method('DELETE')
                                  <button class="btn btn-danger"><i data-feather="trash" class="mr-50"></i><span>Delete</span></button>
                        </form>
                </td>
              </tr>
            @empty
            <tr>
                <td colspan="12" class="text-center">
                    <p class="text-center">No result found for query <strong>{{ request()->query('search') }}</strong></p>
                </td>
            </tr>
            @endforelse
        </tbody>
      </table>
    </div>

  </div>
  {!! $characters->appends(['search' => request()->query('search')])->links() !!}
</div>
</div>
<!-- Table head options end -->

      </div>
    </div>
  </div>
@endsection
