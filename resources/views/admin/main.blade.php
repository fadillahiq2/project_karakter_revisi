<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item mr-auto"><a class="navbar-brand" href="#"><img src="../../app-assets/images/logowk.png" alt="logo wk" height="35">
            <h2 class="brand-text">Wikrama</h2></a></li>
        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
      </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('welcome') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span></a>
        </li>
        </li>
        <li class=" navigation-header"><span data-i18n="Forms &amp; Tables">Forms &amp; Tables</span><i data-feather="more-horizontal"></i>
        </li>
        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('characters.create') }}"><i data-feather="server"></i><span class="menu-title text-truncate" data-i18n="Table">Tambah Data</span></a>
        </li>
         <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('character-dibangun.index') }}"><i data-feather="server"></i><span class="menu-title text-truncate" data-i18n="Table">Tambah Karakter Dibangun</span></a>
        </li>
        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('rombel') }}"><i data-feather="server"></i><span class="menu-title text-truncate" data-i18n="Table">List Data</span></a>
        </li>
    </div>
  </div>
