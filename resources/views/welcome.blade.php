@extends('layouts.default')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body"><!-- Dashboard Ecommerce Starts -->
<section id="dashboard-ecommerce">
<div class="row match-height">


  <!-- Statistics Card -->
  <div class="col-xl-12 col-md-6 col-12">
    <div class="card card-statistics">
      <div class="card-header">
        <h4 class="card-title">Total Data</h4>
      </div>
      <div class="card-body statistics-body">
        <div class="row">
          <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
            <div class="media">
              <div class="avatar bg-light-danger mr-2">
                <div class="avatar-content">
                  <i data-feather="box" class="avatar-icon"></i>
                </div>
              </div>
              <div class="media-body my-auto">
                <h4 class="font-weight-bolder mb-0">{{ $characters }}</h4>
                <a href="{{ route('characters.index') }}" class="card-text font-small-3 mb-0">Karakter</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ Statistics Card -->
</div>

<div class="row match-height">
  <div class="col-lg-4 col-12">
    <div class="row match-height">
      <!-- Bar Chart - Orders -->
      <!--/ Bar Chart - Orders -->

      <!-- Line Chart - Profit -->

      <!--/ Line Chart - Profit -->

      <!-- Earnings Card -->

      <!--/ Earnings Card -->
    </div>
  </div>

  <!-- Revenue Report Card -->

  <!--/ Revenue Report Card -->
</div>

{{-- <div class="row match-height">
  <!-- Company Table Card -->
  <div class="col-lg-12 col-12">
    <div class="card card-company-table">
      <div class="card-body p-0">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Company</th>
                <th>Category</th>
                <th>Views</th>
                <th>Revenue</th>
                <th>Sales</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/toolbox.svg" alt="Toolbar svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Dixons</div>
                      <div class="font-small-2 text-muted">meguc@ruj.io</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-primary mr-1">
                      <div class="avatar-content">
                        <i data-feather="monitor" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Technology</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">23.4k</span>
                    <span class="font-small-2 text-muted">in 24 hours</span>
                  </div>
                </td>
                <td>$891.2</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">68%</span>
                    <i data-feather="trending-down" class="text-danger font-medium-1"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/parachute.svg" alt="Parachute svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Motels</div>
                      <div class="font-small-2 text-muted">vecav@hodzi.co.uk</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-success mr-1">
                      <div class="avatar-content">
                        <i data-feather="coffee" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Grocery</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">78k</span>
                    <span class="font-small-2 text-muted">in 2 days</span>
                  </div>
                </td>
                <td>$668.51</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">97%</span>
                    <i data-feather="trending-up" class="text-success font-medium-1"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/brush.svg" alt="Brush svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Zipcar</div>
                      <div class="font-small-2 text-muted">davcilse@is.gov</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-warning mr-1">
                      <div class="avatar-content">
                        <i data-feather="watch" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Fashion</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">162</span>
                    <span class="font-small-2 text-muted">in 5 days</span>
                  </div>
                </td>
                <td>$522.29</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">62%</span>
                    <i data-feather="trending-up" class="text-success font-medium-1"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/star.svg" alt="Star svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Owning</div>
                      <div class="font-small-2 text-muted">us@cuhil.gov</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-primary mr-1">
                      <div class="avatar-content">
                        <i data-feather="monitor" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Technology</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">214</span>
                    <span class="font-small-2 text-muted">in 24 hours</span>
                  </div>
                </td>
                <td>$291.01</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">88%</span>
                    <i data-feather="trending-up" class="text-success font-medium-1"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/book.svg" alt="Book svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Cafés</div>
                      <div class="font-small-2 text-muted">pudais@jife.com</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-success mr-1">
                      <div class="avatar-content">
                        <i data-feather="coffee" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Grocery</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">208</span>
                    <span class="font-small-2 text-muted">in 1 week</span>
                  </div>
                </td>
                <td>$783.93</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">16%</span>
                    <i data-feather="trending-down" class="text-danger font-medium-1"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/rocket.svg" alt="Rocket svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Kmart</div>
                      <div class="font-small-2 text-muted">bipri@cawiw.com</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-warning mr-1">
                      <div class="avatar-content">
                        <i data-feather="watch" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Fashion</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">990</span>
                    <span class="font-small-2 text-muted">in 1 month</span>
                  </div>
                </td>
                <td>$780.05</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">78%</span>
                    <i data-feather="trending-up" class="text-success font-medium-1"></i>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar rounded">
                      <div class="avatar-content">
                        <img src="https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/app-assets/images/icons/speaker.svg" alt="Speaker svg" />
                      </div>
                    </div>
                    <div>
                      <div class="font-weight-bolder">Payers</div>
                      <div class="font-small-2 text-muted">luk@izug.io</div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <div class="avatar bg-light-warning mr-1">
                      <div class="avatar-content">
                        <i data-feather="watch" class="font-medium-3"></i>
                      </div>
                    </div>
                    <span>Fashion</span>
                  </div>
                </td>
                <td class="text-nowrap">
                  <div class="d-flex flex-column">
                    <span class="font-weight-bolder mb-25">12.9k</span>
                    <span class="font-small-2 text-muted">in 12 hours</span>
                  </div>
                </td>
                <td>$531.49</td>
                <td>
                  <div class="d-flex align-items-center">
                    <span class="font-weight-bolder mr-1">42%</span>
                    <i data-feather="trending-up" class="text-success font-medium-1"></i>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> --}}
</section>
<!-- Dashboard Ecommerce ends -->

      </div>
    </div>
  </div>
@endsection
