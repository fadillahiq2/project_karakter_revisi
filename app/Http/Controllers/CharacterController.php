<?php

namespace App\Http\Controllers;

use App\Models\Character;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exports\CharactersExport;
use App\Models\CharacterDeskripsi;
use App\Models\CharacterDibangun;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->paginate(5);
        }

        return view('characters.index', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function cetak($id)
    {
        $character = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->findOrFail($id);

        return view('characters.cetak', compact('character'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $juduls = CharacterDibangun::get();

        return view('characters.create', compact('juduls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nis' => 'required|numeric|unique:characters',
            'nama' => 'required|max:50',
            'alias_1' => 'required',
            'alias_2' => 'required',
            'alias_3' => 'required',
            'alias_4' => 'required',
            'alias_5' => 'required',
            'catatan' => 'required',
            'penerbitan' => 'required',
            'jurusan' => 'required',
            'kelas' => 'required'
        ]);

        $character = new Character;
        $character->id = Str::random(60);
        $character->nis = $request->nis;
        $character->nama = $request->nama;
        $character->alias_1 = $request->alias_1;
        $character->alias_2 = $request->alias_2;
        $character->alias_3 = $request->alias_3;
        $character->alias_4 = $request->alias_4;
        $character->alias_5 = $request->alias_5;
        $character->catatan = $request->catatan;
        $character->penerbitan = $request->penerbitan;
        $character->jurusan = $request->jurusan;
        $character->kelas = $request->kelas;
        $character->save();

        return redirect()->route('characters.index')->with('success', 'Karakter siswa berhasil dibuat !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $juduls = CharacterDibangun::all();
        $character = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->findOrFail($id);

        return view('characters.edit', compact('character', 'juduls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nis' => 'required|numeric',
            'nama' => 'required|max:50',
            'alias_1' => 'required',
            'alias_2' => 'required',
            'alias_3' => 'required',
            'alias_4' => 'required',
            'alias_5' => 'required',
            'catatan' => 'required',
            'penerbitan' => 'required',
            'jurusan' => 'required',
            'kelas' => 'required'
        ]);

        $character = Character::findOrFail($id);
        $character->update($request->all());

        return redirect()->route('characters.index')->with('success', 'Karakter siswa berhasil diupdate!');
    }

    public function export()
    {
        return Excel::download(new CharactersExport, 'characters.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $character = Character::findOrFail($id);
        $character->delete();

        return redirect()->route('characters.index')->with('success', 'Karakter siswa berhasil dihapus!');
    }

    public function rombel()
    {
        return view('characters.rombel');
    }

    public function kelas_bdp()
    {
        return view('kelas.kelas-bdp');
    }

    public function kelas_htl()
    {
        return view('kelas.kelas-htl');
    }

    public function kelas_mmd()
    {
        return view('kelas.kelas-mmd');
    }

    public function kelas_otkp()
    {
        return view('kelas.kelas-otkp');
    }

    public function kelas_rpl()
    {
        return view('kelas.kelas-rpl');
    }

    public function kelas_tbg()
    {
        return view('kelas.kelas-tbg');
    }

    public function kelas_tkj()
    {
        return view('kelas.kelas-tkj');
    }




    public function kelas_x_tkj()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Teknik Komputer dan Jaringan')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Teknik Komputer dan Jaringan')->paginate(5);
        }

        return view('kelas.kelas-x-tkj', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_tkj()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Teknik Komputer dan Jaringan')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Teknik Komputer dan Jaringan')->paginate(5);
        }

        return view('kelas.kelas-xi-tkj', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_tkj()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Teknik Komputer dan Jaringan')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Teknik Komputer dan Jaringan')->paginate(5);
        }

        return view('kelas.kelas-xii-tkj', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_x_bdp()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Bisnis Daring dan Pemasaran')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Bisnis Daring dan Pemasaran')->paginate(5);
        }

        return view('kelas.kelas-x-bdp', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_bdp()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Bisnis Daring dan Pemasaran')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Bisnis Daring dan Pemasaran')->paginate(5);
        }

        return view('kelas.kelas-xi-bdp', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_bdp()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Bisnis Daring dan Pemasaran')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Bisnis Daring dan Pemasaran')->paginate(5);
        }

        return view('kelas.kelas-xii-bdp', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_x_htl()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Perhotelan')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Perhotelan')->paginate(5);
        }

        return view('kelas.kelas-x-htl', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_htl()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Perhotelan')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Perhotelan')->paginate(5);
        }

        return view('kelas.kelas-xi-htl', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_htl()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Perhotelan')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('jurusan', '=', 'Perhotelan')->where('kelas', '=', 'XII')->paginate(5);
        }

        return view('kelas.kelas-xii-htl', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_x_mmd()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Multimedia')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Multimedia')->paginate(5);
        }

        return view('kelas.kelas-x-mmd', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_mmd()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Multimedia')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Multimedia')->paginate(5);
        }

        return view('kelas.kelas-xi-mmd', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_mmd()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Multimedia')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Multimedia')->paginate(5);
        }

        return view('kelas.kelas-xii-mmd', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_x_otkp()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Otomatisasi Tatakelola dan Perkantoran')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Otomatisasi Tatakelola dan Perkantoran')->paginate(5);
        }

        return view('kelas.kelas-x-otkp', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_otkp()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Otomatisasi Tatakelola dan Perkantoran')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Otomatisasi Tatakelola dan Perkantoran')->paginate(5);
        }

        return view('kelas.kelas-xi-otkp', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_otkp()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Otomatisasi Tatakelola dan Perkantoran')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Otomatisasi Tatakelola dan Perkantoran')->paginate(5);
        }

        return view('kelas.kelas-xii-otkp', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_x_rpl()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Rekayasa Perangkat Lunak')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Rekayasa Perangkat Lunak')->paginate(5);
        }

        return view('kelas.kelas-x-rpl', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_rpl()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Rekayasa Perangkat Lunak')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Rekayasa Perangkat Lunak')->paginate(5);
        }

        return view('kelas.kelas-xi-rpl', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_rpl()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Rekayasa Perangkat Lunak')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Rekayasa Perangkat Lunak')->paginate(5);
        }

        return view('kelas.kelas-xii-rpl', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_x_tbg()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Tataboga')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'X')->where('jurusan', '=', 'Tataboga')->paginate(5);
        }

        return view('kelas.kelas-x-tbg', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xi_tbg()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Tataboga')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XI')->where('jurusan', '=', 'Tataboga')->paginate(5);
        }

        return view('kelas.kelas-xi-tbg', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    public function kelas_xii_tbg()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Tataboga')->where('nis', 'LIKE', "%{$search}%")->orWhere('nama', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = Character::with('alias1', 'alias2', 'alias3', 'alias4', 'alias5')->where('kelas', '=', 'XII')->where('jurusan', '=', 'Tataboga')->paginate(5);
        }

        return view('kelas.kelas-xii-tbg', [
            'characters' => $characters,
        ])
        ->with('i');
    }
}
