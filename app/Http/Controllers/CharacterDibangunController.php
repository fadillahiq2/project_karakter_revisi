<?php

namespace App\Http\Controllers;

use App\Models\CharacterDibangun;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class CharacterDibangunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $search = request()->query('search');

        if($search)
        {
            $characters = CharacterDibangun::where('alias', 'LIKE', "%{$search}%")->paginate(5);
        }else {
            $characters = CharacterDibangun::latest()->paginate(5);
        }

        return view('character_dibangun.index', [
            'characters' => $characters,
        ])
        ->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('character_dibangun.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'alias' => 'required|max:32',
            'judul' => 'required|max:32',
            'deskripsi' => 'required',
        ]);

        CharacterDibangun::create($request->all());

        return redirect()->route('character-dibangun.index')->with('success', 'Karakter Dibangun berhasil dibuat !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $character = CharacterDibangun::findOrFail($id);

        return view('character_dibangun.edit', compact('character'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'alias' => 'required|max:32',
            'judul' => 'required|max:32',
            'deskripsi' => 'required',
        ]);

        $character = CharacterDibangun::findOrFail($id);
        $character->update($request->all());

        return redirect()->route('character-dibangun.index')->with('success', 'Karakter Dibangun berhasil diperbarui !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $character = CharacterDibangun::findOrFail($id);
        $character->delete();

        return redirect()->route('character-dibangun.index')->with('success', 'Karakter Dibangun berhasil dihapus');
    }
}
