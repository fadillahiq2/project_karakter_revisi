<?php

namespace App\Http\Controllers;

use App\Models\Character;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Image;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function welcome()
    {
        $characters = Character::count('id');

        return view('welcome', compact('characters'));
    }
    public function profile()
    {
        return view('profile', [
            'user' => Auth::user()
        ]);
    }

    public function update_profile(Request $request)
    {
       if($request->hasFile('avatar'))
       {
           $avatar = $request->file('avatar');
           $filename = time() . '.' . $avatar->getClientOriginalExtension();
           Image::make($avatar)->save(public_path('/uploads/avatars/' . $filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
       }

        /* Store $imageName name in DATABASE from HERE */

        return back()
            ->with([
                'user' => Auth::user()
            ])
            ->with('success','You have successfully update avatar.');
    }
}
