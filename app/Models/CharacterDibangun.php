<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharacterDibangun extends Model
{
    use HasFactory;

    protected $table = 'character_dibanguns';
    protected $guarded = [];
}
