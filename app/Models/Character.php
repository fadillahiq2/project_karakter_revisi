<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Character extends Model
{
    use HasFactory;

    protected $table = 'characters';
    protected $primaryKey = 'id';
    protected $guarded = [];
    public $incrementing = false;


    public static function getCharacters(){
        $records = DB::table('characters')->select('nis', 'nama', 'integritas', 'religius', 'nasionalis', 'mandiri', 'gotong_royong', 'catatan', 'penerbitan')->get()->toArray();
        return $records;
    }

    public function alias1()
    {
        return $this->belongsTo(CharacterDibangun::class, 'alias_1', 'id');
    }

    public function alias2()
    {
        return $this->belongsTo(CharacterDibangun::class, 'alias_2', 'id');
    }

    public function alias3()
    {
        return $this->belongsTo(CharacterDibangun::class, 'alias_3', 'id');
    }

    public function alias4()
    {
        return $this->belongsTo(CharacterDibangun::class, 'alias_4', 'id');
    }

    public function alias5()
    {
        return $this->belongsTo(CharacterDibangun::class, 'alias_5', 'id');
    }

}
