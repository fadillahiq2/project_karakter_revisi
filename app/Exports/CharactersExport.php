<?php

namespace App\Exports;

use App\Models\Character;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CharactersExport implements FromCollection,WithHeadings,ShouldAutoSize
{
    use Exportable;

    public function headings():array{
        return[
            'NIS',
            'NAMA',
            'INTEGRITAS',
            'RELIGIUS',
            'NASIONALIS',
            'MANDIRI',
            'GOTONG ROYONG',
            'CATATAN',
            'TANGGAL TERBIT',

        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return Character::all();
        return collect(Character::getCharacters());
    }
}
